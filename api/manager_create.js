(function() {
    function getManager(rqst, res, next, helpers) {
        const Managers = require('../data_access/managers').Managers;

        Managers.getManagersList({}, {}, helpers, function(response) {

            res.json(response)
        })
    }


    function createManagers(rqst, res, next, helpers) {
        const Managers = require('../data_access/managers').Managers;

        const bcrypt = require('bcrypt');
        var password = rqst.body.password;

        var username = rqst.body.username;
        var uuid = false;
        var is_verified = false;
        var type = false;
        var has_access = false;


        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (password == "" || password == null) {
            //console.log("Invailid user");
            res.send("*Invailid Password");

        } else if (username.match(mailformat)) {

            let hash = bcrypt.hashSync('myPassword', 10);
            password = hash;
            Managers.createManagers({ "username": username, "password": password, "uuid": uuid, "is_verified": is_verified, "type": type, "has_access": has_access }, {}, helpers, function(response) {
                res.json(response)
            })

        } else {
            //console.log("Invailid Email");
            res.send("*Invailid Email Address");
        }


    }

    exports.Managers = {
        getManager: getManager,
        createManagers: createManagers
    }
})()
